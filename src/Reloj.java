
/**
 *
 * @author joer
 */
public class Reloj {
    private Tiempo r;
    /**
     *Constructura de la clase Reloj. 
     * Comienza a las 0:0:0
     */
    public Reloj () {
        
        r = new Tiempo(0,0,0);
        
    }

    /**
     *
     * @param m minutos que se aumenta el tiempo
     * Incremente los minutos actuales la cifra pasada como parámetro.
     * Si el resultado de la suma es mayor de 59 los minutos se corresponden con el 
     * resto de la división del resultado de la suma entre 60.
     * 
     *Y se aumenta los minutos correspondientes al cociente de la división.
     */
    public void aumentarMinutos (int m) {
        int minutos;
        minutos = r.getMinuto()+m;
        if (minutos > 59) {
            aumentarHoras((int)(minutos / 60));
        }
        r.setMinuto(minutos % 60);
    }

    /**
     *
     * @param s
     */
    public void aumentarSegundos (int s) {
        int segundos;
        segundos = r.getSegundo()+s;
        if (segundos > 59) {
            aumentarMinutos ((int)(segundos / 60));
        }
        r.setSegundo(segundos % 60);
    }

    /**
     *
     * @param h
     */
    public void aumentarHoras (int h) {
        int horas;
        horas = r.getHora()+h;
        if (horas > 23) {
            r.setHora(horas % 24);
        } else {
            r.setHora(horas);
        }
    }



    /**
     *
     * @return
     */
    public String mostrarHora () {
        return Integer.toString(r.getHora())+":"+Integer.toString(r.getMinuto())+":"+Integer.toString(r.getSegundo());
    }

}

