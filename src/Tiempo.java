
public class Tiempo {

    private int hora;

    private int minuto;

    private int segundo;


    public Tiempo (int h, int m, int s) {
        this.hora = h;
        this.minuto = m;
        this.segundo = s;
    }

    public int getHora () {
        return this.hora;
    }

    public void setHora ( int val) {
        this.hora = val;
    }

    public int getMinuto () {
        return minuto;
    }

    public void setMinuto ( int val) {
        this.minuto = val;
    }

    public int getSegundo () {
        return segundo;
    }

    public void setSegundo (int  val) {
        this.segundo = val;
    }

}

