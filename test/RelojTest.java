/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joer
 */
public class RelojTest {
    
    public RelojTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of aumentarMinutos method, of class Reloj.
     */
    @Test
    public void checkAumentarMinutos() {
        System.out.println("Probando... aumentarMinutos");

    }

    /**
     * Test of aumentarSegundos method, of class Reloj.
     */
    @Test
    public void testAumentarSegundos() {
        System.out.println("aumentarSegundos");

    }

    /**
     * Test of aumentarHoras method, of class Reloj.
     */
    @Test
    public void testAumentarHoras() {
        System.out.println("aumentarHoras");

    }

    /**
     * Test of mostrarHora method, of class Reloj.
     */
    @Test
    public void testMostrarHora() {
        System.out.println("mostrarHora");
        Reloj instance = new Reloj();
        String expResult = "";
        String result = instance.mostrarHora();
        assertEquals("0:0:0", result);
        instance.aumentarHoras(22);
        assertEquals("22:0:0", instance.mostrarHora());
        instance.aumentarHoras(5);
        assertEquals("3:0:0", instance.mostrarHora());
                instance.aumentarHoras(25);
        assertEquals("4:0:0", instance.mostrarHora());
                instance.aumentarHoras(25);
                instance.aumentarMinutos(148);
                instance.aumentarSegundos(70);
        assertEquals("7:29:10", instance.mostrarHora());

    }
}
